{ nixpkgs ? (import ./nixpkgs.nix {}) }:

with nixpkgs;
let
  src = nix-gitignore.gitignoreSource [] ./.;
  hs = haskellPackages.callCabal2nix "ghc-homepage" src {};

  scripts = stdenv.mkDerivation {
    name = "ghc-homepage-scripts";
    buildInputs = [ linkchecker ];
    nativeBuildInputs = [ makeWrapper ];
    inherit src;
    installPhase = ''
      mkdir -p $out/bin
      install check.sh $out/bin
      chmod ugo+rx $out/bin/check.sh
      wrapProgram $out/bin/check.sh \
        --prefix PATH : ${linkchecker}/bin:$out/bin
      makeWrapper ${hs}/bin/ghc-homepage $out/bin/ghc-homepage \
        --set LOCALE_ARCHIVE "${glibcLocales}/lib/locale/locale-archive" \
        --set LANG en_US.UTF-8
    '';
  };

in buildEnv { name = "ghc-homepage-utils"; paths = [ scripts openssh rclone ]; }
